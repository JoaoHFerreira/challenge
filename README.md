# Proposta de solução #

Este arquivo servirá como documentação para a solução proposta, nas linhas que seguem
informações referentes à arquitetura serão explicadas.
## Setup
### Orquestrador ###
Para realização da ETL foi escolhido o framework de orquestração [airflow](https://airflow.apache.org/) por conta de algumas características:

* Comunidade ativa e open source: Isso signica que existem pessoas procurando e encontrando soluções.
* Pipelines como código: Toda a orquestração acontece utilizando código python o que facilita na leitura, versionamento e reprodutibilidade dos códigos;
* Excelente documentação: Junto com sua forte comunidade existe também uma excelente documentação com exemplos de como usar.
### Banco de Dados ###
[Postgres](https://www.postgresql.org/) foi o banco de dados escolhido para persistência dos dados pelos seguintes motivos:
* Open source: Postgres existe a mais de 30 anos, e é um dos engines de banco mais potentes do mercado e conta com uma comunidade bastante ativa sendo estável e sem custo.
* Boa interação python X postgres: A linguagem escolihda para o projeto foi `python` o que também favoreceu o uso deste banco, pois existem libs excelentes para manipulação destes recursos

### Linguagem De Programação ###
[Python](https://www.python.org/) foi a linguagem escolhida pelos fatores a seguir:
* Fácil curva de aprendizagem;
* Vasto leque de libs para usos variados;
* Conversa o orquestrador escolhido, também em python;
* Alta velocidade de desenvolvimento;

## Primeira Camada de Carregamento ##
O engine para a construção desta camada foi criado de modo genérico, ele consulta de forma agendada um determinado repositório(foi feito localmente, mas pode ser adaptado para qualquer tpo de storage) os tranforma em csv , com alguns tratamentos e por fim salva os registros num banco transacional, onde as tabelas são criadas dinamicamente.

A imagem abaixo ilustra o pipeline:

![Pipeline](images/pipeline_dados.png)

### Limpeza de dados ###

Na parte da limpeza ele elimina dados do repositório(csv, importante notar que os dados Raw nunca são eliminados) e também todas as tabelas.

### XMLSX para CSV ###
Uma vez que os dados estejam exluídos o processo de ETL se inicia, ele coleta todos os arquivos existentes no repositório RAW e faz os tratamentos necessários para criação de um arquivo csv que forneça informações suficientes para posterior criação de tabelas.

É importante  destacar que o processo já está preparado para recebimento planilhas com abas, ele percorre a planilha inteira e cada "aba" desta vira um arquivo CSV.

O nome definido para criação do CSV foi:
`NOMEDAPLANILHA_NOMEDAABA.csv`
### Criação de tabelas ###
Com os arquivos CSVs devidamente criados, o prcesso de criação de tabelas se inicia. Para geração da tabela e de suas colunas as seguintes regras são utilizadas:
* Nome do arquivo CSV em caixa baixa e sem sua extensão, exemplo:
Se o nome do arquivo for `BaseCargos_Plan1.csv` virará `basecargos_plan1`
* Nome da coluna sem caracteres especiais, espaçamentos substituídos por `_` e letra em caixa baixa, exemplo:
Se o nome da coluna for `Regra de Negócio` virara `regra_de_negocio`

Estas definições foram adotadas para possibilitar a automatização do processo.

### Preenchimento das tabelas ###
Com todas as tabelas criadas, ocorre o preencimento delas. Para preenchimento foi utilizada uma função de cópia de arquivos csv para postgres do próprio banco relacional, sendo uma parte muito performática do processo.

### DAG para realização do pipeline
Usando airflow foi utilizada a [DAG](https://bitbucket.org/JoaoHFerreira/challenge/src/master/dags/nestle_data_sync_dag.py) para gerenciar o pipeline, esta faz o papel de chamar as ETL's acima
e definir a ordem de execução, nela também é possível definir horário fixo de execução e email para dispare em caso de algum erro.

## Próximos passos ##
### Engine de Anonimização dos Dados ###
Estudar e implantar alguma ferramenta de mercado que resolva de modo prático a anonimização de dados.
### Engine Visualização de Dados ###
Após os dados devidamente anonimzados faz sentido pensar numa ferramenta de self BA/BI, que possibilite a democratização e acesso dos dados por diversas da empresa.
### Engine de Consulta dos Metadados ###
Para democratização e acesso a informação de toda a empresa é essencial que existam documentações práticas sobre uso das tabelas, suas relações e até mesmo exemplos de `query`. Para
isso existem algumas ferramentas disponíveis com esse intuito, uma dessas, [amundsen](https://www.amundsen.io/), realiza estas tarefas de modo bastante simples, sendo open source ela já conta com scripts pré determinados
para carregamento de informações de metadados dos [principais data storages](https://github.com/amundsen-io/amundsendatabuilder/tree/8c633072da81b7fb2e343aa31e6e7f9c3bfae07d/example/scripts) do mercado.
### Principais Insights ###
https://docs.google.com/spreadsheets/d/1ZdMagATnpLL5mCeyX4lI_6NsoaoSxoYnqXbe9dO4mXw/edit?usp=sharing
