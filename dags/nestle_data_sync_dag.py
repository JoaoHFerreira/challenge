"""
Dag para sincronizar dados Nestle
"""
from datetime import datetime

from airflow import DAG
from airflow.operators.python_operator import PythonOperator

from lib.etl.clear_files import ClearData
from lib.etl.tables_full_load import TablesFullLoad
from lib.etl.tables_generator import TablesGenerator
from lib.etl.xlsx_to_csv import XlsxToCSV

default_args = {
    "depends_on_past": False,
    "start_date": datetime(2021, 1, 18),
    "email": ["jaycops@gmail.com"],
    "email_on_failure": True,
}

dag = DAG(
    "nestle_data_sync_dag",
    default_args=default_args,
    max_active_runs=1,
    schedule_interval=None,
    catchup=False,
)

dag.doc_md = __doc__


def clear_files():
    ClearData().execute()


clear = PythonOperator(
    task_id="clear",
    depends_on_past=False,
    python_callable=clear_files,
    dag=dag,
)


def convert_xlsx_to_csv():
    XlsxToCSV().execute()


xmlsx_to_csv = PythonOperator(
    task_id="xmlsx_to_csv",
    depends_on_past=False,
    python_callable=convert_xlsx_to_csv,
    dag=dag,
)


def generate_tables():
    TablesGenerator().execute()


generate_table = PythonOperator(
    task_id="generate_table",
    depends_on_past=False,
    python_callable=generate_tables,
    dag=dag,
)


def load_csv():
    TablesFullLoad().execute()


tables_full_load = PythonOperator(
    task_id="tables_full_load",
    depends_on_past=False,
    python_callable=load_csv,
    dag=dag,
)
